#[derive(PartialEq, Default, Debug, Clone)]
pub enum TokenType {
    #[default]
    Empty,
    Identifier,
    Exit,
    IntLiteral,
    Semi,
    OpenBracket,
    CloseBracket,
    Let,
    Equals,
    Multiply,
    Divide,
    Add,
    Subtract,
}

pub fn precedence(token: TokenType) -> Option<i8> {
    match token {
        TokenType::Multiply => Some(1),
        TokenType::Divide => Some(1),
        TokenType::Add => Some(0),
        TokenType::Subtract => Some(0),
        _ => None,
    }
}

#[derive(Default, Debug, Clone)]
pub struct Token {
    pub token: TokenType,
    pub value: Option<String>,
}

impl Token {
    pub fn new(token: TokenType, value: Option<String>) -> Self {
        Self { token, value }
    }
    pub fn exit() -> Self {
        Self::new(TokenType::Exit, None)
    }
    pub fn semi() -> Self {
        Self::new(TokenType::Semi, None)
    }
    pub fn r#let() -> Self {
        Self::new(TokenType::Let, None)
    }
    pub fn equals() -> Self {
        Self::new(TokenType::Equals, None)
    }
    pub fn add() -> Self {
        Self::new(TokenType::Add, None)
    }
    pub fn subtract() -> Self {
        Self::new(TokenType::Subtract, None)
    }
    pub fn multiply() -> Self {
        Self::new(TokenType::Multiply, None)
    }
    pub fn divide() -> Self {
        Self::new(TokenType::Divide, None)
    }
    pub fn open_bracket() -> Self {
        Self::new(TokenType::OpenBracket, None)
    }
    pub fn close_bracket() -> Self {
        Self::new(TokenType::CloseBracket, None)
    }
    pub fn identifier(value: Option<String>) -> Self {
        Self::new(TokenType::Identifier, value)
    }
    pub fn int_literal(value: Option<String>) -> Self {
        Self::new(TokenType::IntLiteral, value)
    }
}

#[derive(Default)]
pub struct Tokenizer {
    pub(crate) source: String,
    index: i32,
}

impl Tokenizer {
    pub fn new(contents: String) -> Self {
        Self {
            source: contents,
            ..Default::default()
        }
    }
    fn peek(&self, offset: i32) -> Option<char> {
        if self.index + offset >= self.source.len() as i32 {
            None
        } else {
            Some(self.source.chars().nth((self.index + offset) as usize)).unwrap()
        }
    }
    fn consume(&mut self) -> char {
        let character = self.source.chars().nth(self.index as usize).unwrap();
        self.index += 1;
        character
    }
    fn exists(&self, offset: i32) -> bool {
        self.peek(offset).is_some()
    }
    fn exists_and_is_alpha(&self, offset: i32) -> bool {
        self.exists(0) && self.is_alpha(offset)
    }
    fn is_alpha(&self, offset: i32) -> bool {
        self.peek(offset).unwrap().is_alphabetic()
    }
    fn exists_and_is_num(&self, offset: i32) -> bool {
        self.exists(0) && self.is_num(offset)
    }
    fn is_num(&self, offset: i32) -> bool {
        self.peek(offset).unwrap().is_numeric()
    }
    fn is_char(&self, char: char, offset: i32) -> bool {
        self.peek(offset).unwrap() == char
    }
    // TODO: Don't use is_whitespace() as is returns true on `_` underscores
    fn is_space(&self, offset: i32) -> bool {
        self.peek(offset).unwrap().is_whitespace()
    }

    pub fn tokenize(&mut self) -> Vec<Token> {
        let mut tokens: Vec<Token> = vec![];
        let mut buf: String = "".to_string();

        while self.exists(0) {
            if self.is_alpha(0) {
                buf.push(self.consume());
                while self.exists_and_is_alpha(0) {
                    buf.push(self.consume());
                }
                match buf.as_str() {
                    "exit" => {
                        tokens.push(Token::exit());
                        buf.clear();
                        continue;
                    }
                    "let" => {
                        tokens.push(Token::r#let());
                        buf.clear();
                        continue;
                    }
                    _ => {
                        tokens.push(Token::identifier(Option::from(buf.clone())));
                        buf.clear();
                        continue;
                    }
                }
            } else if self.is_num(0) {
                buf.push(self.consume());
                while self.exists_and_is_num(0) {
                    buf.push(self.consume());
                }
                tokens.push(Token::int_literal(Option::from(buf.clone())));
                buf.clear();
            } else if self.is_char('(', 0) {
                self.consume();
                tokens.push(Token::open_bracket());
            } else if self.is_char(')', 0) {
                self.consume();
                tokens.push(Token::close_bracket());
            } else if self.is_char(';', 0) {
                self.consume();
                tokens.push(Token::semi());
            } else if self.is_char('=', 0) {
                self.consume();
                tokens.push(Token::equals());
            } else if self.is_char('+', 0) {
                self.consume();
                tokens.push(Token::add());
            } else if self.is_char('-', 0) {
                self.consume();
                tokens.push(Token::subtract());
            } else if self.is_char('*', 0) {
                self.consume();
                tokens.push(Token::multiply());
            } else if self.is_char('/', 0) {
                self.consume();
                tokens.push(Token::divide());
            } else if self.is_space(0) {
                self.consume();
            } else {
                panic!("You messed up!!");
            }
        }
        tokens
    }
}
