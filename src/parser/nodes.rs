use crate::tokenizer::Token;

#[derive(Debug, Clone)]
pub struct TermIntegerLiteral {
    pub literal: Token,
}

#[derive(Debug, Clone)]
pub struct TermIdentifier {
    pub identifier: Token,
}

#[derive(Debug, Clone)]
pub struct TermBrackets {
    pub expression: *mut Expression,
}

#[derive(Debug, Clone)]
pub enum TermVariant {
    IntegerLiteral(*mut TermIntegerLiteral),
    Identifier(*mut TermIdentifier),
    Brackets(*mut TermBrackets),
}

#[derive(Debug, Clone)]
pub struct Term {
    pub variant: TermVariant,
}

#[derive(Debug, Clone)]
pub struct BinaryExpressionMultiplication {
    pub lhs: *mut Expression,
    pub rhs: *mut Expression,
}

#[derive(Debug, Clone)]
pub struct BinaryExpressionDivision {
    pub lhs: *mut Expression,
    pub rhs: *mut Expression,
}

#[derive(Debug, Clone)]
pub struct BinaryExpressionAddition {
    pub lhs: *mut Expression,
    pub rhs: *mut Expression,
}

#[derive(Debug, Clone)]
pub struct BinaryExpressionSubtraction {
    pub lhs: *mut Expression,
    pub rhs: *mut Expression,
}

#[derive(Debug, Clone)]
pub enum BinaryExpressionVariant {
    Multiplication(*mut BinaryExpressionMultiplication),
    Division(*mut BinaryExpressionDivision),
    Addition(*mut BinaryExpressionAddition),
    Subtraction(*mut BinaryExpressionSubtraction),
}

#[derive(Debug, Clone)]
pub struct BinaryExpression {
    pub variant: BinaryExpressionVariant,
}

#[derive(Debug, Copy, Clone)]
pub enum ExpressionVariant {
    Term(*mut Term),
    BinaryExpression(*mut BinaryExpression),
}

#[derive(Debug, Clone)]
pub struct Expression {
    pub variant: ExpressionVariant,
}

#[derive(Debug, Clone)]
pub struct PhraseExit {
    pub expression: *mut Expression,
}

#[derive(Debug, Clone)]
pub struct PhraseLet {
    pub identifier: Token,
    pub expression: *mut Expression,
}

#[derive(Debug, Clone)]
pub enum PhraseVariant {
    Exit(*mut PhraseExit),
    Let(*mut PhraseLet),
}

#[derive(Debug, Clone)]
pub struct Phrase {
    pub variant: PhraseVariant,
}

#[derive(Debug, Clone)]
pub struct Program {
    pub phrases: Vec<*mut Phrase>,
}

impl Program {
    pub fn new() -> Self {
        Self { phrases: vec![] }
    }
}
