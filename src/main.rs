use std::fs;
use std::fs::File;
use std::io::Write;
use std::path::PathBuf;
use std::process::Command;

use clap::Parser;

use crate::generator::Generator;
use crate::parser::{nodes::Program, TokenParser};
use crate::tokenizer::{Token, Tokenizer};

mod arena;
mod generator;
mod parser;
mod tokenizer;

#[derive(clap::Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    path: PathBuf,
}

fn dir_exists(path: &PathBuf) -> bool {
    fs::metadata(path).is_ok()
}

fn main() {
    let cli = Cli::parse();

    let contents = std::fs::read_to_string(&cli.path).expect("Error reading source file!");

    let mut build_dir = PathBuf::new();
    build_dir.push("./example/build/");

    if !dir_exists(&build_dir) {
        if let Err(err) = fs::create_dir(&build_dir) {
            eprintln!("Error creating build directory: {}", err);
        } else {
            println!("Build directory created: {}", build_dir.to_str().unwrap());
        }
    } else {
        println!(
            "Build directory already exists: {}",
            build_dir.to_str().unwrap()
        );
    }

    let mut tokenizer: Tokenizer = Tokenizer::new(contents);
    let tokens: Vec<Token> = tokenizer.tokenize();

    let mut parser: TokenParser = TokenParser::new(tokens);
    let program: Option<Program> = parser.parse_program();

    if program.is_none() {
        panic!("Invalid program");
    }

    let mut generator: Generator = Generator::new(program.unwrap());
    {
        let mut file =
            File::create("./example/build/out.asm").expect("Failed to create output file");
        file.write_all(generator.generate_program().as_ref())
            .expect("Failed to write contents to output file")
    }

    let nasm_out = Command::new("nasm")
        .args(["-felf64", "./example/build/out.asm"])
        .output()
        .expect("Failed to execute 'nasm', is it installed?");

    if nasm_out.status.success() {
        let ld_out = Command::new("ld")
            .args(["-o", "./example/build/out", "./example/build/out.o"])
            .output()
            .expect("Failed to execute 'ld', is it installed?");

        if ld_out.status.success() {
            println!("Successfully compiled chlorine code to `./example/build/out`");
            println!("Do:\n    ./example/build/out  to run the program\nor\n    ./example/build/out; echo $?  to run the program and see the exit code");
        } else {
            let error = String::from_utf8_lossy(&ld_out.stderr);
            eprintln!(
                "Failed to link object file to executable binary:\n{}",
                error
            )
        }
    } else {
        let error = String::from_utf8_lossy(&nasm_out.stderr);
        eprintln!("Failed to compile assembly to object:\n{}", error)
    }
}
