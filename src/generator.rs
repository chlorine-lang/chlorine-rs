use std::collections::HashMap;

use crate::parser::nodes;

struct Variable {
    pub stack_location: i32,
}

impl Variable {
    pub fn new(stack_location: i32) -> Self {
        Self { stack_location }
    }
}

pub struct Generator {
    pub program: nodes::Program,
    output: String,
    stack_size: i32,
    variables: HashMap<String, Variable>,
}

impl Generator {
    pub fn new(program: nodes::Program) -> Self {
        Self {
            program,
            output: String::new(),
            stack_size: 0,
            variables: HashMap::new(),
        }
    }

    pub fn generate_term(&mut self, term: &nodes::Term) {
        match &term.variant {
            nodes::TermVariant::IntegerLiteral(term_integer_literal) => unsafe {
                self.write(format!(
                    "    mov rax, {}",
                    (*(*term_integer_literal)).literal.value.clone().unwrap()
                ));
                self.push("rax");
            },
            nodes::TermVariant::Identifier(term_identifier) => unsafe {
                if !self.variables.contains_key(
                    (*(*term_identifier))
                        .identifier
                        .value
                        .clone()
                        .unwrap()
                        .as_str(),
                ) {
                    panic!(
                        "Undeclared identifier: {}",
                        (*(*term_identifier)).identifier.value.clone().unwrap()
                    )
                }
                let variable = self
                    .variables
                    .get(
                        (*(*term_identifier))
                            .identifier
                            .value
                            .clone()
                            .unwrap()
                            .as_str(),
                    )
                    .unwrap();
                let offset: String = format!(
                    "QWORD [rsp + {}]\n",
                    (self.stack_size - variable.stack_location - 1) * 8
                );
                self.push(offset.as_str());
            },
            nodes::TermVariant::Brackets(term_brackets) => unsafe {
                self.generate_expression(&*(*(*term_brackets)).expression);
            },
        }
    }

    pub fn generate_binary_expression(&mut self, binary_expression: &nodes::BinaryExpression) {
        match &binary_expression.variant {
            nodes::BinaryExpressionVariant::Multiplication(mul) => unsafe {
                self.generate_expression(&*(*(*mul)).rhs);
                self.generate_expression(&*(*(*mul)).lhs);
                self.pop("rax");
                self.pop("rbx");
                self.output.push_str("    mul rbx\n");
                self.push("rax");
            },
            nodes::BinaryExpressionVariant::Division(div) => unsafe {
                self.generate_expression(&*(*(*div)).rhs);
                self.generate_expression(&*(*(*div)).lhs);
                self.pop("rax");
                self.pop("rbx");
                self.output.push_str("    div rbx\n");
                self.push("rax");
            },
            nodes::BinaryExpressionVariant::Addition(add) => unsafe {
                self.generate_expression(&*(*(*add)).rhs);
                self.generate_expression(&*(*(*add)).lhs);
                self.pop("rax");
                self.pop("rbx");
                self.output.push_str("    add rax, rbx\n");
                self.push("rax");
            },
            nodes::BinaryExpressionVariant::Subtraction(sub) => unsafe {
                self.generate_expression(&*(*(*sub)).rhs);
                self.generate_expression(&*(*(*sub)).lhs);
                self.pop("rax");
                self.pop("rbx");
                self.output.push_str("    sub rax, rbx\n");
                self.push("rax");
            },
        }
    }

    pub fn generate_expression(&mut self, expression: &nodes::Expression) {
        match &expression.variant {
            nodes::ExpressionVariant::Term(term) => unsafe {
                self.generate_term(&**term);
            },
            nodes::ExpressionVariant::BinaryExpression(binary_expression) => unsafe {
                self.generate_binary_expression(&**binary_expression)
            },
        }
    }

    pub fn generate_phrase(&mut self, phrase: &nodes::Phrase) {
        match &phrase.variant {
            nodes::PhraseVariant::Exit(phrase_exit) => unsafe {
                self.generate_expression(&*(*(*phrase_exit)).expression);
                self.write(String::from("    mov rax, 60"));
                self.pop("rdi");
                self.write(String::from("    syscall"));
            },
            nodes::PhraseVariant::Let(phrase_let) => unsafe {
                if self
                    .variables
                    .contains_key((*(*phrase_let)).identifier.value.clone().unwrap().as_str())
                {
                    panic!(
                        "Undeclared identifier: {}",
                        (*(*phrase_let)).identifier.value.clone().unwrap()
                    )
                }
                self.variables.insert(
                    (*(*phrase_let)).identifier.value.clone().unwrap(),
                    Variable::new(self.stack_size),
                );
                self.generate_expression(&*(*(*phrase_let)).expression);
            },
        }
    }

    pub fn generate_program(&mut self) -> String {
        self.write(String::from("global _start"));
        self.write(String::from("_start:"));
        for phrase in &self.program.phrases.clone() {
            unsafe { self.generate_phrase(&*(*phrase)) };
        }
        self.write(String::from("    mov rax, 60"));
        self.write(String::from("    mov rdi, 0"));
        self.write(String::from("    syscall"));

        self.output.clone()
    }

    fn write(&mut self, string: String) {
        self.output.push_str(format!("{}{}", string, "\n").as_str());
    }
    fn push(&mut self, registry: &str) {
        self.write(format!("    push {}", registry));
        self.stack_size += 1;
    }
    fn pop(&mut self, registry: &str) {
        self.write(format!("    pop {}", registry));
        self.stack_size -= 1;
    }
}
