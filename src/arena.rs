use std::alloc::{alloc, dealloc, Layout};
use std::sync::Mutex;

type Byte = u8;

#[derive(Debug)]
pub struct ArenaAllocator {
    arena_size: usize,
    buffer: *mut Byte,
    offset: Mutex<usize>,
}

impl ArenaAllocator {
    pub fn new(bytes: usize) -> Self {
        let layout = Layout::from_size_align(bytes, std::mem::align_of::<Byte>()).unwrap();
        let buffer = unsafe { alloc(layout) };
        Self {
            arena_size: bytes,
            buffer,
            offset: Mutex::new(0),
        }
    }

    pub fn alloc<T>(&mut self) -> *mut T {
        let layout = Layout::new::<T>();
        let mut offset = self.offset.lock().unwrap();
        let result = unsafe { self.buffer.add(*offset) as *mut T };
        *offset += layout.size();
        result
    }
}

impl Drop for ArenaAllocator {
    fn drop(&mut self) {
        let layout =
            Layout::from_size_align(self.arena_size, std::mem::align_of::<Byte>()).unwrap();
        unsafe { dealloc(self.buffer, layout) };
    }
}
