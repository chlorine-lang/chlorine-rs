use crate::arena::ArenaAllocator;
use crate::tokenizer::{self, Token, TokenType};

use self::nodes::Term;

pub mod nodes;

#[derive(Debug)]
pub struct TokenParser {
    pub tokens: Vec<Token>,
    index: i32,
    allocator: ArenaAllocator,
}

impl TokenParser {
    pub fn new(tokens: Vec<Token>) -> Self {
        Self {
            tokens,
            index: 0,
            allocator: ArenaAllocator::new(1024 * 1024 * 4),
        }
    }

    fn parse_term(&mut self) -> Option<*mut nodes::Term> {
        if let Some(integer_literal) = self.try_consume(TokenType::IntLiteral, None) {
            unsafe {
                let term_integer_literal: *mut nodes::TermIntegerLiteral =
                    self.allocator.alloc::<nodes::TermIntegerLiteral>();
                (*term_integer_literal).literal = integer_literal;
                let term: *mut nodes::Term = self.allocator.alloc::<nodes::Term>();
                (*term).variant = nodes::TermVariant::IntegerLiteral(term_integer_literal);
                Some(term)
            }
        } else if let Some(identifier) = self.try_consume(TokenType::Identifier, None) {
            unsafe {
                let expression_identifier: *mut nodes::TermIdentifier =
                    self.allocator.alloc::<nodes::TermIdentifier>();
                (*expression_identifier).identifier = identifier;
                let term: *mut nodes::Term = self.allocator.alloc::<nodes::Term>();
                (*term).variant = nodes::TermVariant::Identifier(expression_identifier);
                Some(term)
            }
        } else if let Some(open_bracket) = self.try_consume(TokenType::OpenBracket, None) {
            let expression = self.parse_expression(0);
            if expression.is_none() {
                panic!("Expected expression!");
            }
            self.try_consume(TokenType::CloseBracket, Some("Expected `)`"));
            let mut term_bracket = self.allocator.alloc::<nodes::TermBrackets>();
            let mut term = self.allocator.alloc::<nodes::Term>();
            unsafe {
                (*term_bracket).expression = expression.unwrap();
                (*term).variant = nodes::TermVariant::Brackets(term_bracket);
            }
            Some(term)
        } else {
            None
        }
    }

    pub fn parse_expression(&mut self, minimum_precedence: i8) -> Option<*mut nodes::Expression> {
        let term_lhs: Option<*mut Term> = self.parse_term();

        if term_lhs.is_none() {
            return None;
        }

        let mut expression_lhs = self.allocator.alloc::<nodes::Expression>();
        unsafe { (*expression_lhs).variant = nodes::ExpressionVariant::Term(term_lhs.unwrap()) }

        loop {
            let current_token: Option<Token> = self.peek(0);
            let mut precedence: Option<i8>;
            if current_token.is_some() {
                precedence = tokenizer::precedence(current_token.unwrap().token);
                if precedence.is_none() || precedence.unwrap() < minimum_precedence {
                    break;
                }
            } else {
                break;
            }

            let operator: Token = self.consume();
            let next_minimum_precedence: i8 = precedence.unwrap() + 1;
            let expression_rhs = self.parse_expression(next_minimum_precedence);
            if expression_rhs.is_none() {
                panic!("Unable to parse expression");
            }
            let mut expression = self.allocator.alloc::<nodes::BinaryExpression>();
            let mut expression_lhs2 = self.allocator.alloc::<nodes::Expression>();
            match operator.token {
                TokenType::Multiply => unsafe {
                    let mut multiply = self
                        .allocator
                        .alloc::<nodes::BinaryExpressionMultiplication>();
                    (*expression_lhs2).variant = (*expression_lhs).variant;
                    (*multiply).lhs = expression_lhs2;
                    (*multiply).rhs = expression_rhs.unwrap();
                    (*expression).variant =
                        nodes::BinaryExpressionVariant::Multiplication(multiply);
                },
                TokenType::Divide => unsafe {
                    let mut divide = self.allocator.alloc::<nodes::BinaryExpressionDivision>();
                    (*expression_lhs2).variant = (*expression_lhs).variant;
                    (*divide).lhs = expression_lhs2;
                    (*divide).rhs = expression_rhs.unwrap();
                    (*expression).variant = nodes::BinaryExpressionVariant::Division(divide);
                },
                TokenType::Add => unsafe {
                    let mut add = self.allocator.alloc::<nodes::BinaryExpressionAddition>();
                    (*expression_lhs2).variant = (*expression_lhs).variant;
                    (*add).lhs = expression_lhs2;
                    (*add).rhs = expression_rhs.unwrap();
                    (*expression).variant = nodes::BinaryExpressionVariant::Addition(add);
                },
                TokenType::Subtract => unsafe {
                    let mut sub = self.allocator.alloc::<nodes::BinaryExpressionSubtraction>();
                    (*expression_lhs2).variant = (*expression_lhs).variant;
                    (*sub).lhs = expression_lhs2;
                    (*sub).rhs = expression_rhs.unwrap();
                    (*expression).variant = nodes::BinaryExpressionVariant::Subtraction(sub);
                },
                _ => {
                    break;
                }
            }
            unsafe {
                (*expression_lhs).variant = nodes::ExpressionVariant::BinaryExpression(expression)
            }
        }

        Some(expression_lhs)
    }

    pub fn parse_phrase(&mut self) -> Option<*mut nodes::Phrase> {
        if self.exists_and_is(TokenType::Exit, 0) && self.exists_and_is(TokenType::OpenBracket, 1) {
            self.consume();
            self.consume();
            unsafe {
                let phrase_exit = self.allocator.alloc::<nodes::PhraseExit>();
                if let Some(expression) = self.parse_expression(0) {
                    (*phrase_exit).expression = expression;
                } else {
                    panic!("Invalid expression!");
                }
                self.try_consume(TokenType::CloseBracket, Some("Expected `)`"));
                self.try_consume(TokenType::Semi, Some("Expected `;`"));
                let phrase = self.allocator.alloc::<nodes::Phrase>();
                (*phrase).variant = nodes::PhraseVariant::Exit(phrase_exit);
                Some(phrase)
            }
        } else if self.exists_and_is(TokenType::Let, 0)
            && self.exists_and_is(TokenType::Identifier, 1)
            && self.exists_and_is(TokenType::Equals, 2)
        {
            self.consume();
            unsafe {
                let phrase_let = self.allocator.alloc::<nodes::PhraseLet>();
                (*phrase_let).identifier = self.consume();
                self.consume();
                if let Some(expression) = self.parse_expression(0) {
                    (*phrase_let).expression = expression;
                } else {
                    panic!("Invalid expression!");
                }
                self.try_consume(TokenType::Semi, Some("Expected `;`"));
                let phrase = self.allocator.alloc::<nodes::Phrase>();
                (*phrase).variant = nodes::PhraseVariant::Let(phrase_let);
                Some(phrase)
            }
        } else {
            None
        }
    }

    pub fn parse_program(&mut self) -> Option<nodes::Program> {
        let mut program: nodes::Program = nodes::Program::new();
        while self.exists(0) {
            if let Some(phrase) = self.parse_phrase() {
                program.phrases.push(phrase);
            } else {
                panic!(
                    "Invalid phrase {:?}",
                    self.peek(0).expect("No phrase").token
                );
            }
        }
        Some(program)
    }

    fn peek(&self, offset: i32) -> Option<Token> {
        let index: i32 = self.index + offset;

        if index >= 0 && index < self.tokens.len() as i32 {
            Some(self.tokens[index as usize].clone())
        } else {
            None
        }
    }

    fn consume(&mut self) -> Token {
        let token = self.tokens[self.index as usize].clone();
        self.index += 1;
        token
    }

    fn try_consume(&mut self, token_type: TokenType, error: Option<&str>) -> Option<Token> {
        if self.exists_and_is(token_type, 0) {
            Some(self.consume())
        } else {
            if error.is_some() {
                panic!("{}", error.unwrap())
            } else {
                None
            }
        }
    }

    fn exists(&self, offset: i32) -> bool {
        self.peek(offset).is_some()
    }
    fn is(&self, token_type: TokenType, offset: i32) -> bool {
        self.peek(offset).unwrap().token == token_type
    }
    fn exists_and_is(&self, token_type: TokenType, offset: i32) -> bool {
        self.exists(0) && self.is(token_type, offset)
    }
}
