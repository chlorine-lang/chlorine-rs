$$
\begin{align}
	[\text{prog}] &\to [\text{phrase}]^*\\
	[\text{phrase}] &\to 
	\begin{cases}
		exit([\text{expr}]);\\
		let\space\text{ident}=[\text{expr}];
	\end{cases}\\
	
	[\text{expr}] &\to 
	\begin{cases}
		\text{int\_lit}\\
		\text{ident}\\
		[\text{binary\_expr}]\\
	\end{cases}\\
	
	[\text{binary\_expr}] &\to 
	\begin{cases}
		[\text{expr}] \times [\text{expr}]
		& \text{prec} = 1\\
		[\text{expr}] \div [\text{expr}]
		& \text{prec} = 1\\
		[\text{expr}] + [\text{expr}]
		& \text{prec} = 0\\
		[\text{expr}] - [\text{expr}]
		& \text{prec} = 0\\
	\end{cases}
\end{align}
$$