# Chlorine Lang
Chlorine Lang or Chlorine is a hobby language that will hopefully be able to be self-hosted in the future


## Prerequesites
Currently this compiler only supports Linux 

Requires:
- `nasm`
- `ld`
- `cargo`

## Usage

```bash
git clone https://gitlab.com/chlorine-lang/chlorine-rs.git
cd chlorine-rs
cargo build
./target/debug/chl "./example/src/main.chl"
./example/build/out; echo $?
```

## Syntax
As of `8/26/2023` there are only 2 keywords implemented, `let` and `exit()`

### let
`let` assigns a variable to an identifier, for example:
```ts
let myvar = 100;
```

### exit
`exit()` takes in an integer and stops program execution with the value of the integer as the exit code of the program. For example the following code will return `200` as the exit status. You can see the exit status by doing `echo $?` in bash after running your program.
```ts
exit(200);
```
`exit()` can also take in a variable as an argument
```ts
let exitcode = 202;
exit(exitcode);

/* When compiled and ran:
$ chl "main.chl"
Successfully compiled chlorine code to `./example/build/out`"
...
$ ./example/build/out; echo $?
202
*/
```
